package br.com.archery.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.archery.enums.SituationEnterprise;
import br.com.archery.interfaces.EnterpriseRepository;
import br.com.archery.model.Enterprise;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class EnterpriseDAO implements EnterpriseRepository{
	
	@PersistenceContext
	private EntityManager manager;
	private Session session;
	
	@Override
	public void insert(Enterprise enterprise){
		manager.persist(enterprise);
	}
	
	@Override
	public Enterprise find(Long id){
		return manager.find(Enterprise.class, id);
	}

	@Override
	public void remove(Long id) {
		manager.remove(find(id));
	}

	@Override
	public void logicExclusion(Long id) {
		Enterprise enterprise = find(id);
		enterprise.setSituation(SituationEnterprise.DISABLED);
		update(enterprise);
	}

	@Override
	public void update(Enterprise enterprise) {
		manager.merge(enterprise);
	}

	@Override
	public List<Enterprise> getAll() {
		getSession();
		Criteria criteria = session.createCriteria(Enterprise.class);
		criteria.add(Restrictions.eq("situation", SituationEnterprise.ACTIVE))
				.add(Restrictions.isNotEmpty("products"));
		
		return criteria.list();
	}
	
	private void getSession(){
		session = manager.unwrap(Session.class);
	}
}
