package br.com.archery.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.archery.enums.SituationProduct;
import br.com.archery.interfaces.ProductsRepository;
import br.com.archery.model.Product;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class ProductDAO implements ProductsRepository{

	@PersistenceContext
	private EntityManager manager;
	private Session session;
	
	@Override
	public void insert(Product product) {
		manager.persist(product);
	}

	@Override
	public Product find(Long id) {
		return (Product) manager.find(Product.class, id);
	}

	@Override
	public void remove(Long id) {
		manager.remove(find(id));		
	}

	@Override
	public void logicExclusion(Long id) {
		Product product = find(id);
		product.setSituation(SituationProduct.DESCONTINUED);
		update(product);
	}

	@Override
	public void update(Product product) {
		manager.merge(product);
	}

	@Override
	public List<Product> getAll() {
		return manager.createQuery("select p from Product p",Product.class).getResultList();
	}

	@Override
	public List<Product> getProductsByEnterprise(Long id) {
		getSession();
		Criteria criteria = session.createCriteria(Product.class);
		criteria.add(Restrictions.eq("enterprise.id", id));
		criteria.addOrder(Order.asc("name"));
		return criteria.list();
	}
	
	private void getSession(){
		session = manager.unwrap(Session.class);
	}

	
}
