package br.com.archery.interfaces;

import java.util.List;

import br.com.archery.model.Product;

public interface ProductsRepository extends CrudRepository<Product>{
	
	public List<Product> getProductsByEnterprise(Long id);
}
