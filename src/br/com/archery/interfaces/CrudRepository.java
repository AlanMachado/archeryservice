package br.com.archery.interfaces;

import java.util.List;


public interface CrudRepository<T> {

	void insert(T t);
	T find(Long id);
	void remove(Long id);
	void logicExclusion(Long id);
	void update(T t);
	List<T> getAll();
}
