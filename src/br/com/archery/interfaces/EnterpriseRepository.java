package br.com.archery.interfaces;

import br.com.archery.model.Enterprise;

public interface EnterpriseRepository extends CrudRepository<Enterprise>{

}
