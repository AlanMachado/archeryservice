package br.com.archery.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import br.com.archery.enums.SituationEnterprise;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@Component
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="Enterprise")
public class Enterprise {
	
	@Expose
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="enterpriseGenerator")
	@SequenceGenerator(name="enterpriseGenerator", sequenceName="enterpriseSequence")
	private Long id;
	
	@Expose
	private String name;
	
	@Expose
	@Enumerated(EnumType.STRING)
	private SituationEnterprise situation;
	
	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=false, mappedBy="enterprise", fetch=FetchType.LAZY)
	private List<Product> products;
	
	public Enterprise() {}
	
	public Enterprise(Long id, String name, SituationEnterprise situation) {
		this.id = id;
		this.name = name;
		this.situation = situation;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SituationEnterprise getSituation() {
		return situation;
	}
	public void setSituation(SituationEnterprise situation) {
		this.situation = situation;
	}
	
	public Enterprise build(){
		this.setId(1L);
		this.setName("TESTANDO AUTO");
		this.setSituation(SituationEnterprise.ACTIVE);
		return this;
	}
	
	public String toJSON(){
		Gson gson = new GsonBuilder()
		.excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enterprise other = (Enterprise) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
