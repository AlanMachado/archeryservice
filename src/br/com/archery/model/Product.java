package br.com.archery.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.archery.enums.SituationProduct;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(name="Product")
@Entity
public class Product {

	@Expose
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="productGenerator")
	@SequenceGenerator(name="productGenerator", sequenceName="productSequence")
	private Long id;
	
	@ManyToOne
	private Enterprise enterprise;
	
	@Expose
	private String name;
	
	@Expose
	private Integer year;
	
	@Expose
	@Enumerated(EnumType.STRING)
	private SituationProduct situation;
	
	public Product(){}
	
	public Product(Long id, String name, Integer year, SituationProduct situation) {
		this.id = id;
		this.name = name;
		this.year = year;
		this.situation = situation;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public SituationProduct getSituation() {
		return situation;
	}
	public void setSituation(SituationProduct situation) {
		this.situation = situation;
	}
	
	public String toJSON(){
		Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(this);
	}
	
	public Product build(){
		this.setId(1L);
		this.setName("Condor Ve");
		this.setSituation(SituationProduct.CONTINUED);
		this.setYear(LocalDateTime.now().getYear());
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
