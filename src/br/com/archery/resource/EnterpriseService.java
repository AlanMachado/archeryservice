package br.com.archery.resource;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.archery.interfaces.EnterpriseRepository;
import br.com.archery.model.Enterprise;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
@Path("/enterprises")
public class EnterpriseService {
	
	@Autowired
	private EnterpriseRepository enterpriseDAO;
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String findEnterprise(@PathParam("id") Long id){
		Enterprise e = enterpriseDAO.find(id);
		return e.toJSON();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML) // lembrar de sempre mandar no header: Content-Type: application/xml
	public Response insertEnterprise(Enterprise enterprise){
		enterpriseDAO.insert(enterprise);
		return Response.created(URI.create("enterprises/".concat(enterprise.getId().toString()))).build();
	}
	
	@Path("/remove/{id}")
	@DELETE
	public Response removeEnterprise(@PathParam("id") Long id){
		enterpriseDAO.remove(id);
		return Response.ok().build();
	}
	
	@Path("/disable/{id}")
	@PUT
	public Response disableEnterprise(@PathParam("id") Long id){
		enterpriseDAO.logicExclusion(id);
		return Response.ok().build();
	}
	
	@Path("/update")
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response update(Enterprise enterprise){
		enterpriseDAO.update(enterprise);
		return Response.created(URI.create("enterprises/".concat(enterprise.getId().toString()))).build();
	}
	
	@Path("/listAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllEnterprises(){
		List<Enterprise> enterprises = enterpriseDAO.getAll();
		return listToJson(enterprises);
	}
	
	private String listToJson(List<Enterprise> list){
		Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation().create();
		
		return gson.toJson(list);
	}
	
}
