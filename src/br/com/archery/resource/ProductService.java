package br.com.archery.resource;

import java.net.URI;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.archery.interfaces.ProductsRepository;
import br.com.archery.model.Product;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
@Path("/products")
public class ProductService {

	@Autowired
	private ProductsRepository productDAO;
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String findProduct(@PathParam("id") Long id) throws Exception{
		Product p = productDAO.find(id);
		return p.toJSON();
	}
	
	@POST
	public Response insert(Product product){
		productDAO.insert(product);
		return Response.created(URI.create("products/".concat(product.getId().toString()))).build();
	}
	
	@Path("/remove/{id}")
	@DELETE
	public Response removeProduct(@PathParam("id") Long id){
		productDAO.remove(id);
		return Response.ok().build();
	}
	
	@Path("/descontinue/{id}")
	@PUT
	public Response descontinueProduct(@PathParam("id") Long id){
		productDAO.logicExclusion(id);
		return Response.ok().build();
	}
	
	@Path("/update/{id}")
	@PUT
	public Response update(Product product){
		productDAO.update(product);
		return Response.created(URI.create("products/".concat(product.getId().toString()))).build();
	}
	
	@Path("/fromEnterprise/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getProductsFromEnterprise(@PathParam("id") Long id){
		List<Product> products = productDAO.getProductsByEnterprise(id);
	
		return listToJson(products);
	}
	
	private String listToJson(List<Product> list){
		Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation().create();
		
		return gson.toJson(list);
	}
}
