package br.com.archery.test.server;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {
	public static HttpServer server;
	
	public static void main(String[] args) throws IOException {
		startaServidor();
	    System.in.read();
	    server.stop();
	}
	
	public static void startaServidor(){
		URI uri = URI.create("http://localhost:8080/archeryServer");
		ResourceConfig config = new ResourceConfig().packages("br.com.archery");
		server = GrizzlyHttpServerFactory.createHttpServer(uri, config);			
	}
}
