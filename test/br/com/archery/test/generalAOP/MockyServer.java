package br.com.archery.test.generalAOP;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.After;
import org.junit.Before;

import br.com.archery.test.server.Servidor;

public class MockyServer {
	protected WebTarget target;
	protected Client client;
	
	@Before
	public void startServer(){
		Servidor.startaServidor();
		ClientConfig config = new ClientConfig();
		config.register(new LoggingFilter());
		this.client = ClientBuilder.newClient(config);
		this.target = client.target("http://localhost:8080/archeryServer");
	}
	
	@After
	public void killServer(){
		Servidor.server.stop();
	}
}
