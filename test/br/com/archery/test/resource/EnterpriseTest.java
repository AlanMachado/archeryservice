package br.com.archery.test.resource;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Test;

import br.com.archery.model.Enterprise;
import br.com.archery.test.generalAOP.MockyServer;

public class EnterpriseTest extends MockyServer{
	
	@Test
	public void testIfInsertEnterprise(){
		Enterprise enterprise = new Enterprise();
		
		Entity<Enterprise> entity = Entity.entity(enterprise.build(), MediaType.APPLICATION_XML);
		Response response = target.path("/enterprises").request().post(entity);
		Assert.assertEquals(201, response.getStatus());
		String location = response.getHeaderString("Location");
		Enterprise enterpriseFromLocation = client.target(location).request().get(Enterprise.class);
		Assert.assertEquals(enterprise, enterpriseFromLocation);
	}
	
}
